// to check whether a given string is palindrome or not, dynamic

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
char *s;
int i,j,flag=0;

void main()
{
	s=(int*)malloc(sizeof(int));
	printf("\nenter a string: ");
	gets(s);
	printf("%s",s);
	for(i=0,j=strlen(s)-1;i<j;i++,j--)
	{
		if(*(s+i)!=*(s+j))
		{
			flag=1;
			break;
		}
	}
	if(flag == 0)
		printf("\npalindrome\n");
	else
		printf("\nnot palindrome\n");
}
